
# Multi-omic statistics workflow for the MiBiPa/2-HP manuscript

This directory contains the main multi-omic statistics and visualisations related to the MiBiPa study, with a focus on 2-hydroxypyridine, as presented in the manuscript by Trezzi, Aho et al. (2022).

Contents:
- **_targets.R** -- File implementing the main targets-based analysis workflow
- **R/** -- Directory with R functions needed for the targets workflow
- **mibipa_2hp_figs.Rmd** -- R Markdown document for drawing the main manuscript figures. Output: exported separate pdfs for each of the four main figures.
- **mibipa_2hp_extended_data.Rmd** -- R Markdown document for putting together the Extended Data for the manuscript. Output: one pdf combining all the Extended Tables and Figures (for initial review) and separate exports for each figure and table (in pdf format; will need conversion to JPEG/TIFF/EPS to finalise).
- **renv.lock** -- Lock file created by the `renv` package containing details of the required packages
- **renv/activate.R** -- Auto-loader created by the `renv` package
